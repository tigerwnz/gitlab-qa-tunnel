default['gitlab-qa-tunnel']['fqdn'] = node['fqdn']
default['gitlab-qa-tunnel']['ssl_certificate'] = ''
default['gitlab-qa-tunnel']['ssl_key'] = ''

default['gitlab-qa-tunnel']['nginx'] = {}
default['gitlab-qa-tunnel']['nginx']['client_max_body_size'] = '1024m'

default['nginx']['default_site_enabled'] = false
