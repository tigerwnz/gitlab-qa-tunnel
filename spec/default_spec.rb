require 'spec_helper'

describe 'gitlab-qa-tunnel::default' do
  cached(:chef_run) do
    ChefSpec::SoloRunner.new { |node|
      node.automatic['fqdn'] = 'gitlab-qa-tunnel.example.com'
    }.converge(described_recipe)
  end

  it 'creates configuration of qa-tunnel site' do
    expect(chef_run).to create_template('/etc/nginx/sites-available/qa-tunnel').with(
      owner: 'root',
      group: 'root',
      mode: 0644
    )
  end

  it 'sets default nginx configuration parameters' do
    expect(chef_run).to render_file('/etc/nginx/sites-available/qa-tunnel').with_content{ |content|
      expect(content).to match 'client_max_body_size 1024m;'
    }
  end

  context 'when nginx configuration is specified' do
    cached(:chef_run_conf) do
      ChefSpec::SoloRunner.new { |node|
        node.automatic['fqdn'] = 'gitlab-qa-tunnel.example.com'
        node.override['gitlab-qa-tunnel'] = {
          'nginx' => {
            'client_max_body_size' => '100m'
          }
        }
      }.converge(described_recipe)
    end

    it 'sets specified configuration details' do
      expect(chef_run_conf).to render_file('/etc/nginx/sites-available/qa-tunnel').with_content{ |content|
        expect(content).to match 'client_max_body_size 100m;'
      }
    end
  end

  it 'creates SSL files' do
    expect(chef_run).to create_file('/etc/ssl/gitlab-qa-tunnel.example.com.crt').with(
      owner: 'root',
      group: 'root',
      mode: 0644
    )
    expect(chef_run).to create_file('/etc/ssl/gitlab-qa-tunnel.example.com.key').with(
      owner: 'root',
      group: 'root',
      mode: 0600
    )
  end
end
