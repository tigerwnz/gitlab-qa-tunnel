name             'gitlab-qa-tunnel'
maintainer       'GitLab Inc.'
maintainer_email 'dgriffith@gitlab.com'
license          'MIT'
description      'Installs/Configures gitlab-qa-tunnel'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.5'

depends 'chef_nginx', '~> 6.1'
depends 'chef-vault', '~> 3.0'
depends 'gitlab-vault', '~> 0.2'
