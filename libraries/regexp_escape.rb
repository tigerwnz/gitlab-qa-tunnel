module RegexpEscape
  def regexp_escape(value)
    value.gsub('.', '\.')
  end
end
