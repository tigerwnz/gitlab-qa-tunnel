# Fetch secrets from Chef Vault
qa_tunnel_conf = GitLab::Vault.get(node, 'gitlab-qa-tunnel')

include_recipe 'chef_nginx'

template "#{node['nginx']['dir']}/sites-available/qa-tunnel" do
  source 'nginx-site-qa-tunnel.erb'
  owner  'root'
  group  'root'
  mode   0644
  helpers RegexpEscape
  variables(
    conf: qa_tunnel_conf
  )
  notifies :restart, resources(:service => 'nginx')
end

nginx_site 'qa-tunnel' do
  enable true
end

file "/etc/ssl/#{qa_tunnel_conf['fqdn']}.crt" do
  content qa_tunnel_conf['ssl_certificate']
  owner 'root'
  group 'root'
  mode 0644
  notifies :restart, resources(:service => 'nginx'), :delayed
end

file "/etc/ssl/#{qa_tunnel_conf['fqdn']}.key" do
  content qa_tunnel_conf['ssl_key']
  owner 'root'
  group 'root'
  mode 0600
  notifies :restart, resources(:service => 'nginx'), :delayed
end
