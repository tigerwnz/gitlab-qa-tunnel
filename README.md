gitlab-qa-tunnel Cookbook
=======================

Usage
-----
#### gitlab-qa-tunnel::default

Just include `gitlab-qa-tunnel` in your node's `run_list`:

```json
{
  "name":"my_node",
  "run_list": [
    "recipe[gitlab-qa-tunnel]"
  ]
}
```
